function TracktModel(Restangular, $q, PrototypeModel) {

	var endpoint = 'tracks';
	class Track extends PrototypeModel {
		constructor(object = {}) {
			let defaults  = {
				id : null,
				album : null,
				artists : '',
				available_markets: null,
				disc_number : [],
				duration_ms : null,
				explicit : null,
				external_ids : '',
				href : '',
				name : '',
				popularity : '',
				preview_url : null,
				track_number : '',
				type : null,
				uri : '',
			};
			super(endpoint,defaults,object);
			//super.extendModel(object);
		}


		_requestInterceptor(element, operation, what) {

			console.log(` on element: ${what} , operation: ${operation}`);
			if (what == endpoint) {
				if (operation === "put" || operation === 'post') {

				}
			}
			return element;
		}

	}

	return Track;
}

export default ['Restangular', '$q', 'PrototypeModel', TracktModel];

