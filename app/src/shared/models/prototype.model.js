function PrototypeModel(Restangular, $q) {
	class Model {
		constructor(endpoint,defaults = {},object = {}) {
			this.endpoint = endpoint;
			Object.assign(this, defaults,object);

			if (!this.restangularized)  {
				Restangular.restangularizeElement(null, this, this.endpoint);
			}


		}

		extendRestangularModel() {
			Restangular.extendModel(this.endpoint, modelExtender.bind(this));
			function modelExtender(model) {
				let extender =  Object.assign(angular.copy(this), model);
				return extender;
			}
			return this;
		}

		setRestangularInterceptor() {

			Restangular.addRequestInterceptor(this._requestInterceptor.bind(this));
			return this;
		}
	}

	return Model;
}

export default ['Restangular', '$q', PrototypeModel];