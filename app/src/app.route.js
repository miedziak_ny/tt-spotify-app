export default function($locationProvider, $stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/index/home");

    $stateProvider
        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "./src/app.template.html",
        })
        .state('index.home', {
            url: "/home",
            views : {
                content : {
                    component: "homePageIndex"
                },
            },
        })
	    .state('index.spotify-search', {
		    url: "/spotify-search/:queryString",
		    views : {
			    content : {
				    component: "spotifySearchPage"
			    },

		    },
            resolve : {
                queryString : ['$transition$',function ($transition$) {
                    return $transition$.params().queryString;

                }]
            }
	    })




}