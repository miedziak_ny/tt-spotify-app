// Load libraries
import angular from 'angular';

import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'lodash';
import 'restangular';
import 'angular-ui-router';

import * as AppConfig from 'src/app.config';

import AppRoute from 'src/app.route';
import AppComponent from 'src/app.component';

import HomePage from 'src/home-page/home-page.module';
import Spotify from 'src/spotify/spotify.module';

import PrototypeModel from 'src/shared/models/prototype.model';
import PrototypeService from 'src/shared/services/prototype.service';




export default angular.module( 'starter-app', [ 'ngMaterial','restangular','ui.router', HomePage.name,Spotify.name ] )
    .config(AppConfig.mdConfig)
    .config(AppConfig.restangularConfig)
    .config(AppRoute)
	.run(['ArtistModel',AppConfig.modelConfig])
    .component(AppComponent.name, AppComponent.config)
	.factory("PrototypeModel", PrototypeModel)
	.service("PrototypeService", PrototypeService)

