import SpotifySearchBoxController from './spotify-search-box.controller'
import './spotify-search-box.css!';
export default {
    name: 'spotifySearchBox',
    config: {
        bindings: {
            queryString : "<"
        },
        templateUrl: 'src/spotify/components/spotify-search-box/spotify-search-box.view.html',
        controller: ['SpotifySearchService','$rootScope','ArtistModel',SpotifySearchBoxController]
    }
};
