class SpotifySearchBoxController  {
    constructor(SpotifySearchService,$rootScope,ArtistModel) {
        this._$rootScope = $rootScope;
        this._SpotifySearchService = SpotifySearchService;
        this._ArtistModel = ArtistModel;
        this.searchType= 'artist';
	    this.searchInput = '';

    }
    $onInit() {
        if(this.queryString) {
            this.search(this.queryString);
            this.searchInput = this.queryString;

        }
    }

    search(query) {


	    this._SpotifySearchService
            .search(query,this.searchType)
            .then(searchFinishEmitter.bind(this))
            .catch(error);

        function searchFinishEmitter(response) {
            let nodePluralization = this.searchType+'s';
	        this._$rootScope.$broadcast('on-search-finished',this.searchType,response[nodePluralization].items.map(item => new this._ArtistModel(item)));

        }

        function error() {
            console.error('error while geting artists');
        }
    }

}
export default SpotifySearchBoxController;

