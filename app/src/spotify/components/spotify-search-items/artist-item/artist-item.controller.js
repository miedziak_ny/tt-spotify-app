class ArtistItemController  {
    constructor() {
	    this.TRACK_PER_ARTIST = 5;
        this.artist.tracks  = [];

    }
    $onInit() {

    }
	showArtistTracks() {
        this.artist.isExpanded = !this.artist.isExpanded;
        this.artist
            .getTopTracks()
            .then(setTracks.bind(this))
            .catch(error);

        function setTracks(response) {
            this.artist.tracks = response.tracks;
        }

        function error(error) {
            console.error(error);
        }
    }

}
export default ArtistItemController;

