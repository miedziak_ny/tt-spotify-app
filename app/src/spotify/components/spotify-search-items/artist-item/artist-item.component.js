import ArtistItemController from './artist-item.controller';
import './artist-item.css!';

export default {
    name: 'artistItem',
    config: {
        bindings: {
            artist : "<"
        },
        templateUrl: 'src/spotify/components/spotify-search-items/artist-item/artist-item.view.html',
        controller: [ArtistItemController]
    }
};
