import TrackItemController from './track-item.controller';
import './track-item.css!';

export default {
    name: 'trackItem',
    config: {
        bindings: {
            track : "<"
        },
        templateUrl: 'src/spotify/components/spotify-search-items/track-item/track-item.view.html',
        controller: ['ngAudio',TrackItemController]
    }
};
