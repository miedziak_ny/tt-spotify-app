class TrackItemController {
    constructor(ngAudio) {
        this.state = "PAUSE";
        this._ngAudio = ngAudio;
    }

    setState(state) {
        if(!this.ngAudioObject)
            this.ngAudioObject=this._ngAudio.load(this.track.preview_url);

        switch(state) {
        case "PLAY" :
            this.play();
            this.state = "PLAY";
            break;

        case "PAUSE":
            this.pause();
            this.state = "PAUSE";
            break;

        default:
            this.state = "PLAY";

        }
    }

    pause() {
        if(this.ngAudioObject)
            this.ngAudioObject.pause();
    }

    play() {
        if(this.ngAudioObject)
            this.ngAudioObject.play();
    }

    $onInit() {

    }

}
export default TrackItemController;

