import SpotifySearchResultsController from './spotify-search-results.controller'
export default {
    name: 'spotifySearchResults',
    config: {
        bindings: {
	        resultsList : "<"
        },
        templateUrl: 'src/spotify/components/spotify-search-results/spotify-search-results.view.html',
        controller: ['$scope',SpotifySearchResultsController]
    }
};
