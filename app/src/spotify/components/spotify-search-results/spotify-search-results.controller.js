class SpotifySearchResultsController  {
    constructor($scope) {
        this.$scope =  $scope;

    }

	onSearchFinished(event,type,data) {
    	this.resultsType = type;
        this.resultsList = data;
    }

    $onInit() {
        this.$scope.$on('on-search-finished',this.onSearchFinished.bind(this))
        this.resultsList = [];
    }

}
export default SpotifySearchResultsController;

