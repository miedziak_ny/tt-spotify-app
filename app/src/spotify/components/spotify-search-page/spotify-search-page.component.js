import SpotifySearchPageController from './spotify-search-page.controller'
export default {
    name: 'spotifySearchPage',
    config: {
        bindings: {
            queryString: "<"
        },
        templateUrl: 'src/spotify/components/spotify-search-page/spotify-search-page.view.html',
        controller: [SpotifySearchPageController]
    }
};
