function SpotifySearchService(Restangular) {

	class service {
		constructor() {
			this.endpoint = 'search';
			let service = Restangular.service(this.endpoint);
			Object.assign(this,service);
			this.Restangular = Restangular;
		}

		search(q,type,params={}) {
			let queryParams = Object.assign({q,type},params);
			return this.get('',queryParams);
		}
	}

	return new service();
}




export default ['Restangular',SpotifySearchService];
