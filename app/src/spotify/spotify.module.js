import 'angular-audio';
import SpotifySearchBox from './components/spotify-search-box/spotify-search-box.component';
import SpotifySearchPage from './components/spotify-search-page/spotify-search-page.component';
import SpotifySearchResults from './components/spotify-search-results/spotify-search-results.component';
import ArtistItem from './components/spotify-search-items/artist-item/artist-item.component';
import TrackItem from './components/spotify-search-items/track-item/track-item.component';

import SpotifySearchService from './services/spotify-search.service';

export default angular
    .module("Spotify", ['ngMaterial','ngAudio'])

    .component(SpotifySearchBox.name, SpotifySearchBox.config)
    .component(SpotifySearchPage.name, SpotifySearchPage.config)
    .component(SpotifySearchResults.name, SpotifySearchResults.config)
    .component(ArtistItem.name, ArtistItem.config)
    .component(TrackItem.name, TrackItem.config)
    .service("SpotifySearchService", SpotifySearchService)





