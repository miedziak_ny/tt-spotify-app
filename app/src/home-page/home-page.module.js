import HomePageIndex from './components/home-page-index/home-page-index.component';
import HomePageIndexService from './components/home-page-index/home-page-index.service';

import ArtistsService from 'src/shared/services/artists.service';
import ArtistModel from 'src/shared/models/artist.model';

export default angular
    .module("HomePage", ['ngMaterial'])

    .component(HomePageIndex.name, HomePageIndex.config)
    .service("HomePageIndexService", HomePageIndexService)
    .service("ArtistsService", ArtistsService)
    .factory("ArtistModel", ArtistModel)




